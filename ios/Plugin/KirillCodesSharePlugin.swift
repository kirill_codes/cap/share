//  KirillCodesSharePlugin.swift
//  Plugin

//  Created by Kirill

import Capacitor
import Foundation
import LinkPresentation
import UIKit

class KirillCodesItemSource: NSObject, UIActivityItemSource {
    var linkMetadata: LPLinkMetadata = LPLinkMetadata();
    var text: String;
    var title: String;

    init(title: String, url: URL, text: String, provider: NSItemProvider?) {
        linkMetadata.iconProvider = NSItemProvider(
          contentsOf: Bundle.main.url(forResource: "logo", withExtension: "png")
        );
        linkMetadata.imageProvider = NSItemProvider(
          contentsOf: Bundle.main.url(forResource: "logo", withExtension: "png")
        );
        linkMetadata.title = title;
        linkMetadata.url = url;
        linkMetadata.originalURL = URL(fileURLWithPath: text);

        self.text = text;
        self.title = title;

        super.init();
    }
  }

extension KirillCodesItemSource {
    func activityViewControllerLinkMetadata(_ activityViewController: UIActivityViewController) -> LPLinkMetadata? {
      return linkMetadata;
    }

    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
      return "";
    }

    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
      return "\(self.title) and \(self.text)\n \n\(linkMetadata.url!)";
    }

    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
      return linkMetadata.title ?? "KirillCodes";
    }
}


@objc(KirillCodesSharePlugin)
public class KirillCodesSharePlugin: CAPPlugin {

    @objc func isShareAvailable (_ call: CAPPluginCall) {
      call.resolve([ "available": true, ]);
    }

    @objc public func shareGeneral ( activityItems: [ Any ], call: CAPPluginCall ) {
      DispatchQueue.main.async {
        let controller = UIActivityViewController(
          activityItems: activityItems,
          applicationActivities: nil
        );

        controller.completionWithItemsHandler = { (activityType, completed, _ returnedItems, activityError) in
          if activityError != nil {
            call.resolve([
              "ok": false,
              "message": activityError?.localizedDescription ?? "Something went wrong"
            ]);

            return;
          }

          if completed {
            call.resolve([ "ok": true, "message": activityType?.rawValue ?? "Completed", ]);
            return;
          } else {
            call.resolve([ "ok": true, "message": "Canceled", ])
            return
          }
        }

        DispatchQueue.main.async {
          if self.bridge?.viewController?.presentedViewController != nil {
            return;
          }

          self.notifyListeners("ready", data: [ "ok": true, ])
          self.bridge?.viewController?.present(controller, animated: true, completion: nil);
        }

      }
    }

    @objc func shareFile (_ call: CAPPluginCall) {
      let file = call.getString("file") ?? "";
      let title = call.getString("title") ?? "";

      let fileToShare = FileManager.default.temporaryDirectory.appendingPathComponent(title);
      guard let dataObj = Data(base64Encoded: file) else {
        call.resolve([ "ok": false, "message": "Corrupt Data" ]);
        return;
      }
      do {
        try dataObj.write(to: fileToShare)
      } catch {
        call.resolve([ "ok": false, "message": "Corrupt Data" ]);
      }

      self.shareGeneral(activityItems: [ fileToShare ], call: call)

    }
    }

    @objc func shareUrl (_ call: CAPPluginCall) {
        let text = call.getString("text") ?? "";
        let title = call.getString("title") ?? "";
        guard let urlString = call.getString("url") else {
          call.resolve([ "ok": false, "message": "URL must be provided" ]);
          return;
        }
        let url = URL(string: urlString)!;

        DispatchQueue.main.async {
            let activityItems: [ Any ] = [ KirillCodesItemSource(
              title: title,
              url: url,
              text: text,
              provider: nil
            ), ];

            self.shareGeneral(activityItems: activityItems, call: call)
        }
    }
}
