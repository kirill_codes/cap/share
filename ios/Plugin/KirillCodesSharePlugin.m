//
//  KirillCodesSharePlugin.m
//  Plugin
//
//  Created by Kirill Chernik on 9/6/22.
//

#import <Foundation/Foundation.h>
#import <Capacitor/Capacitor.h>

CAP_PLUGIN(KirillCodesSharePlugin, "KirillCodesShare",
    CAP_PLUGIN_METHOD(isShareAvailable, CAPPluginReturnPromise);
    CAP_PLUGIN_METHOD(shareUrl, CAPPluginReturnPromise);
    CAP_PLUGIN_METHOD(shareFile, CAPPluginReturnPromise);
)
