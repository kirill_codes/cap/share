
import { registerPlugin } from "@capacitor/core";

import type { KirillCodesSharePlugin } from "./definitions";

const KirillCodesShare = registerPlugin<KirillCodesSharePlugin>("KirillCodesShare", {
  async web ( ) {
    return import("./web").then(function getShareWeb ( module ) {
      return new module.KirillCodesShareWeb();
    });
  },
});

export * from "./definitions";
export { KirillCodesShare };
