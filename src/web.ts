/* eslint-disable @typescript-eslint/ban-ts-comment */

import type { KirillCodesSharePlugin } from "./definitions";

import { WebPlugin } from "@capacitor/core";
import type { ListenerCallback, PluginListenerHandle } from "@capacitor/core";

export class KirillCodesShareWeb extends WebPlugin implements KirillCodesSharePlugin {

  public async isShareAvailable ( ): Promise<{
    available: boolean;
  }> {
    return Promise.resolve({ available: false, });
  }

  public async shareUrl (
    _options: { text: string; title: string; url: string; }
  ): Promise<{
    ok: boolean; message?: string;
  }> {
    return Promise.resolve({
      ok: false, message: "Not impleented on Web",
    });
  }

  public async shareFile ( _options: {
    file: string; title: string;
  } ): Promise<{
    ok: boolean; message?: string;
  }> {
    return Promise.resolve({
      ok: false, message: "Not impleented on Web",
    });
  }

  public async addListener (
    _eventName: string, _listenerFunc: ListenerCallback
    // @ts-expect-error
  ): Promise<PluginListenerHandle> & PluginListenerHandle {
    return <PluginListenerHandle><unknown>null;
  }

}
