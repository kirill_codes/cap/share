
import type { ListenerCallback, PluginListenerHandle } from "@capacitor/core";

export interface KirillCodesSharePlugin {
  isShareAvailable ( ): Promise<{
    available: boolean;
  }>;

  shareUrl ( options: { text: string; title: string; url: string; } ): Promise<{
    ok: boolean; message?: string;
  }>;

  shareFile ( options: { file: string; title: string; } ): Promise<{
    ok: boolean; message?: string;
  }>;

  addListener(
    eventName: string,
    handler: ListenerCallback
  ): Promise<PluginListenerHandle> & PluginListenerHandle;
}
