package com.kirill_codes.plugin.share

import android.app.Activity
import android.content.*
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Base64
import android.util.Log
import androidx.activity.result.ActivityResult
import com.getcapacitor.JSObject
import com.getcapacitor.Plugin
import com.getcapacitor.PluginCall
import com.getcapacitor.PluginMethod
import com.getcapacitor.annotation.ActivityCallback
import com.getcapacitor.annotation.CapacitorPlugin
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*


@CapacitorPlugin(name = "KirillCodesShare")
class KirillCodesSharePlugin : Plugin() {
    private val broadcastReceiver: BroadcastReceiver? = null
    private val chosenComponent: ComponentName? = null;
    private var thumbnail: ClipData? = null;

    override fun load ( ) {
        val thumbnailGetter = getClipDataThumbnail();

        thumbnailGetter?.let {
            thumbnail = it
        }
    }

    override fun handleOnDestroy() {
        if (broadcastReceiver != null) {
            activity.unregisterReceiver(broadcastReceiver)
        }
    }


    @ActivityCallback
    private fun activityResult ( call: PluginCall, activityResult: ActivityResult ) {
        val shared = JSObject()
        val result = JSObject();
        if (activityResult.resultCode == Activity.RESULT_CANCELED) {
            result.put("ok", true);
            result.put("message", "Canceled")
            call.resolve(result)
        } else {
            result.put("ok", true);
            result.put("message", chosenComponent?.packageName ?: "")
            call.resolve(result)
        }
        notifyListeners("ready", shared)
    }

    @PluginMethod
    fun isShareAvailable ( call: PluginCall ) {
        call.resolve(JSObject().put("available", true))
    }

    @PluginMethod
    fun shareUrl ( call: PluginCall ) {

        val result = JSObject();
        val title = call.getString("title", "")
        val text = call.getString("text", "")
        val url = call.getString("url", "")

        if (url == "null") {
            result.put("ok", false);
            result.put("message", "URL must be provided");
            call.resolve(result);
        }

        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(
            Intent.EXTRA_TEXT, """
             $text
             $url
            """.trimIndent()
        )
        intent.clipData = thumbnail;
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        intent.putExtra(Intent.EXTRA_TITLE, title)
        intent.putExtra(Intent.EXTRA_SUBJECT, title)
        intent.setTypeAndNormalize("text/plain");

        startActivityForResult(call, Intent.createChooser(intent, title), "activityResult")
    }

    @PluginMethod
    fun shareFile ( call: PluginCall ) {
        val title = call.getString("title", "");
        val fileString = call.getString("file", "");
        val type = call.getString("type", "application/pdf");

        val intent = Intent(Intent.ACTION_SEND)

        if (fileString == null || fileString.isEmpty()) {
            return
        }

        val file: File = File(getCacheDir(), title);
        try {
            FileOutputStream(file).use { fos ->
                val decodedData: ByteArray =
                    Base64.decode(fileString, Base64.DEFAULT)
                fos.write(decodedData)
                fos.flush()
            }
        } catch (e: IOException) {
            Log.e(logTag, e.message!!)
            return
        } catch (e: IllegalArgumentException) {
            Log.e(logTag, e.message!!)
            return
        }
        val fileProviderName = context.packageName + ".fileprovider";
        val contentUri = androidx.core.content.FileProvider.getUriForFile(context, fileProviderName, file)

        intent.putExtra(Intent.EXTRA_STREAM, contentUri)
        intent.setTypeAndNormalize(type)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        startActivityForResult(call, Intent.createChooser(intent, title), "activityResult")
    }


    private fun getCacheDir(): File? {
        val cacheDir = File(context.filesDir, "kirill_codes")
        if (!cacheDir.exists()) {
            cacheDir.mkdirs()
        } else {
            if (cacheDir.listFiles() != null) {
                for (f in Objects.requireNonNull(cacheDir.listFiles())) {
                    f.delete()
                }
            }
        }
        return cacheDir
    }

    private fun saveThumbnailImage ( ): Uri {
        val fileProviderName = context.packageName + ".fileprovider";
        val cacheDir = getCacheDir()
        val file = File(cacheDir, "logo.png")
        val stream = FileOutputStream("$cacheDir/logo.png")

        val icon = BitmapFactory.decodeResource(
            context.resources, R.drawable.logo
        );

        icon.compress(Bitmap.CompressFormat.PNG, 100, stream)
        stream.close();

        return androidx.core.content.FileProvider.getUriForFile(context, fileProviderName, file);

    }

    private fun getClipDataThumbnail ( ): ClipData? {
        return try {
            ClipData.newUri(context.contentResolver, "null", saveThumbnailImage());
        } catch (e: FileNotFoundException) {
            Log.e("SHARE", e.localizedMessage ?: "getClipDataThumbnail FileNotFoundException");
            null;
        } catch (e: IOException) {
            Log.e("SHARE", e.localizedMessage ?: "getClipDataThumbnail IOException");
            null;
        }
    }
}
